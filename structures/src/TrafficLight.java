import java.util.Scanner;

public class TrafficLight {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Please enter colour of traffic light:");
        String traffic = myScanner.next();

        if (traffic.equals("red")) {
            System.out.println("STOP!");
        } else if (traffic.equals("green")) {
            System.out.println("Walk!");
        } else {
            System.out.println("Look out for vehicles!");
        }

    }


}




